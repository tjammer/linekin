(type ease-kind (#linear #circ-in #circ-out #cubic-out #quad-inout))

(type anim {:kind ease-kind :src float :diff float :animtime float :currtime float})

(external sqrt (fun float float))

(defn ease-circ-in [anim]
  (let [t (/. (.currtime anim) (.animtime anim))]
    (-> - (.diff anim)
        (*. (-. (sqrt (-. 1.0 (*. t t))) 1.0))
        (+. (.src anim)))))

(defn ease-circ-out [anim]
  (def t (-. (/. (.currtime anim) (.animtime anim)) 1.0))
  (-> (-. 1.0 (*. t t )) sqrt (*. (.diff anim)) (+. (.src anim))))

(defn ease-cubic-out [anim]
  (def t (-. (/. (.currtime anim) (.animtime anim)) 1.0))
  (-> (*. t t t) (+. 1.0) (*. (.diff anim)) (+. (.src anim))))

(defn ease-linear [anim]
  (+. (.src anim) (*. (.diff anim) (/. (.currtime anim) (.animtime anim)))))

(defn ease-quad-inout [anim]
  (let [t (/. (.currtime anim) (*. (.animtime anim) 0.5))]
    (if (<. t 1.0)
      (-> (*. (.diff anim) 0.5) (*. (*. t t)) (+. (.src anim)))
      (-> (*. (.diff anim) -0.5)
          (*. (-. (*. (-. t 1.0) (-. t 3.0)) 1.0))
          (+. (.src anim))))))

(defn ease [anim]
  (match (.kind anim)
    (#linear (ease-linear anim))
    (#circ-in (ease-circ-in anim))
    (#circ-out (ease-circ-out anim))
    (#cubic-out (ease-cubic-out anim))
    (#quad-inout (ease-quad-inout anim))))
