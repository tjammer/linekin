##
# linekin
# @version 0.1

.PHONY: all clean run

all: linekin

MAIN = linekin.smu
OBJS = raylib.o easings.o

linekin: $(MAIN) $(OBJS)
	$(SCHMU) --cc -L. --cc -lraylib --cc -lm --cc -lX11 --cc -lGL $(MAIN)

$(OBJS): %.o: %.smu
	$(SCHMU) -m $<

run: all
	./linekin

clean:
	rm *.o linekin *.smi
